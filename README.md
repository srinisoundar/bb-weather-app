# BbWeatherApp

Weather app developed using [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Description

This app is built with the below components
* weather-card - shows the forecast details in a card view
* dashboard - this is the home page of the app consumes weather-card to show the forecasts for the cities
* weather-hourly - shows the forecast for a city hourly/3 hr/5 day forecast based on the api 

The app has a shared module which has shared components & services
* error-alert - to show the error on api failure
* spinner - to show up the spinner on async calls
* open-weather-map - REST apis using the open weather map services
* wind-speed-service - to show up the icon based on the wind speed

Unit tests are written for each component and services.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

