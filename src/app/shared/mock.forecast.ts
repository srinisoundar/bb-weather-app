export const forecastsMock = {
  list: [
    {
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      main: {
        temp: 30.69,
        pressure: 1009,
        humidity: 35,
        temp_min: 28.89,
        temp_max: 32.22
      },
      wind: {
        speed: 8.7,
        deg: 190
      },
      dt: 1559491738,
      id: 2759794,
      name: 'Amsterdam'
    },
    {
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      main: {
        temp: 30.45,
        pressure: 1012,
        humidity: 30,
        temp_min: 28.33,
        temp_max: 32.22
      },
      wind: {
        speed: 5.7,
        deg: 240
      },
      dt: 1559491818,
      id: 2988507,
      name: 'Paris'
    }
  ]
};


export const forecastsHourlyMock = {
  list: [{
    dt: 1559800800,
    main: {
      temp: 13.9,
      temp_min: 13.9,
      temp_max: 13.9,
      pressure: 1009.49,
      sea_level: 1009.49,
      grnd_level: 1009.46,
      humidity: 84,
      temp_kf: 0
    },
    weather: [
      {
        id: 500,
        main: 'Rain',
        description: 'light rain',
        icon: '10d'
      }
    ],
    clouds: {
      all: 75
    },
    wind: {
      speed: 6.96,
      deg: 235.253
    },
    dt_txt: '2019-06-06 06:00:00'
  },
    {
      dt: 1559811600,
      main: {
        temp: 14.45,
        temp_min: 14.45,
        temp_max: 14.45,
        pressure: 1011.3,
        sea_level: 1011.3,
        grnd_level: 1011.37,
        humidity: 79,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 5
      },
      wind: {
        speed: 7.83,
        deg: 223.4
      },
      dt_txt: '2019-06-06 09:00:00'
    },
    {
      dt: 1559822400,
      main: {
        temp: 14.66,
        temp_min: 14.66,
        temp_max: 14.66,
        pressure: 1012.67,
        sea_level: 1012.67,
        grnd_level: 1013.07,
        humidity: 75,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 5
      },
      wind: {
        speed: 8.06,
        deg: 236.328
      },
      dt_txt: '2019-06-06 12:00:00'
    }]
};
