import { TestBed } from '@angular/core/testing';

import { WindSpeedService } from './wind-speed.service';

describe('WindSpeedService', () => {
  let service: WindSpeedService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(WindSpeedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getWindScaleIcon', () => {
    it('should return `gentle breeze icon 953` on speed = 4m/s', () => {
      expect(service.getWindScaleIcon(4)).toEqual('953');
    });

    it('should return `moderate breeze icon 954` on speed = 14mph', () => {
      expect(service.getWindScaleIcon(14, 'mph')).toEqual('954');
    });
  });
});
