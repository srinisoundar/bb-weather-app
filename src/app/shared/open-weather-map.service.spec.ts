import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {OpenWeatherMapService} from './open-weather-map.service';
import {forecastsMock, forecastsHourlyMock} from './mock.forecast';
import {environment} from '../../environments/environment';

describe('OpenWeatherMapService', () => {
  let service: OpenWeatherMapService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(OpenWeatherMapService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getGroupWeatherForecast', () => {
    const id = '2759794,2988507';
    const groupUrl = `${environment.baseUrl}/group?id=${id}&units=metric&mode=json&appid=${environment.apiKey}`;
    it('should return an Observable<Forecast[]>', () => {
      const dummyForecasts = forecastsMock;

      service.getGroupWeatherForecast({id}).subscribe(forecastsData => {
        expect(forecastsData.length).toBe(2);
        expect(forecastsData).toBe(dummyForecasts.list);
      });

      const req = httpTestingController.expectOne(groupUrl);
      expect(req.request.method).toBe('GET');
      expect(req.request.params.get('id')).toBe('2759794,2988507');
      expect(req.request.params.get('units')).toBe('metric');
      expect(req.request.params.get('mode')).toBe('json');
      expect(req.request.params.get('appid')).toBe(environment.apiKey);
      req.flush(dummyForecasts);
    });

    it('expect forecast to be empty array when not available', () => {
      service.getGroupWeatherForecast({id}).subscribe(forecastsData => {
        expect(forecastsData.length).toBe(0);
      });
      const req = httpTestingController.expectOne(groupUrl);
      req.flush({});
    });

    it('should return an technical error when the server returns a 500', () => {
      service.getGroupWeatherForecast({id}).subscribe(
        () =>  fail('should have failed with the 500 error'),
        (error) => {
          expect(error).toBe('Technical error; please try again later.');
        });

      const req = httpTestingController.expectOne(groupUrl);
      req.flush('500 error', { status: 500, statusText: 'Internal server error' });
    });

    it('should return an network error when the server returns a 404', () => {
      service.getGroupWeatherForecast({id}).subscribe(
        () =>  fail('should have failed with the 404 error'),
        (error) => {
          expect(error).toBe('Network error; please try again later.');
        });

      const req = httpTestingController.expectOne(groupUrl);
      const e = new ErrorEvent('network error');
      req.flush(e, { status: 404, statusText: 'Not Found' });
    });
  });

  describe('#getWeatherHourlyForecast', () => {
    const hourlyUrl = `${environment.baseUrl}/forecast?id=2759794&units=metric&mode=json&appid=${environment.apiKey}`;
    it('should return an Observable<ForecastHourly[]>', () => {
      const dummyForecastsHourly = forecastsHourlyMock;

      service.getWeatherHourlyForecast({id: '2759794'}).subscribe(forecastsData => {
        const data = {
          dt: forecastsHourlyMock.list[forecastsHourlyMock.list.length - 1].dt,
          forecasts: forecastsHourlyMock.list
        };
        expect(forecastsData.length).toBe(1);
        expect(forecastsData[0].dt).toBe(data.dt);
        expect(forecastsData[0].forecasts.length).toBe(3);
      });

      const req = httpTestingController.expectOne(hourlyUrl);
      expect(req.request.method).toBe('GET');
      expect(req.request.params.get('id')).toBe('2759794');
      expect(req.request.params.get('units')).toBe('metric');
      expect(req.request.params.get('mode')).toBe('json');
      expect(req.request.params.get('appid')).toBe(environment.apiKey);
      req.flush(dummyForecastsHourly);
    });

    it('expect forecast to be empty array when not available', () => {
      service.getWeatherHourlyForecast({id: '2759794'}).subscribe(forecastsData => {
        expect(forecastsData.length).toBe(0);
      });
      const req = httpTestingController.expectOne(hourlyUrl);
      req.flush({});
    });


    it('should return an technical error when the server returns a 500', () => {
      service.getWeatherHourlyForecast({id: '2759794'}).subscribe(
        () =>  fail('should have failed with the 500 error'),
        (error) => {
          expect(error).toBe('Technical error; please try again later.');
        });

      const req = httpTestingController.expectOne(hourlyUrl);
      req.flush('500 error', { status: 500, statusText: 'Internal server error' });
    });

    it('should return an network error when the server returns a 404', () => {
      service.getWeatherHourlyForecast({id: '2759794'}).subscribe(
        () =>  fail('should have failed with the 404 error'),
        (error) => {
          expect(error).toBe('Network error; please try again later.');
        });

      const req = httpTestingController.expectOne(hourlyUrl);
      const e = new ErrorEvent('network error');
      req.flush(e, { status: 404, statusText: 'Not Found' });
    });
  });
});
