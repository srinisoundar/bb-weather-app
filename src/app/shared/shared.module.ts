import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpinnerComponent} from './spinner/spinner.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ErrorAlertComponent} from './error-alert/error-alert.component';

@NgModule({
  declarations: [SpinnerComponent, ErrorAlertComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    SpinnerComponent,
    ErrorAlertComponent
  ]
})
export class SharedModule { }
