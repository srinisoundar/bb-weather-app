interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

interface Main {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}

interface Wind {
  speed: number;
  deg: number;
}

export interface Forecast {
  id: number;
  name: string;
  dt: number;
  weather: Weather[];
  main: Main;
  wind: Wind;
}

export interface ForecastHourly {
  dt: number;
  forecasts: Forecast[];
}
