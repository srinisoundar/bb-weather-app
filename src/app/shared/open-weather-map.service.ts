import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

import {environment} from '../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Forecast, ForecastHourly} from './forecast.model';

@Injectable({
  providedIn: 'root'
})
export class OpenWeatherMapService {
  private weatherGroupUrl = `${environment.baseUrl}/group`;
  private weatherHourlyUrl = `${environment.baseUrl}/forecast`;

  constructor(private http: HttpClient) {}

  /**
   * Method to fetch the forecast for list of cities
   * @param options - params for the request
   * @return list of forecastsMock
   */
  getGroupWeatherForecast(options = {}): Observable<Forecast[]> {
    return this.http.get<any>(this.weatherGroupUrl, {params: this.getHttpParams(options)})
      .pipe(
        map(({list = []}: {list?: Forecast[]} = {}) => list),
        catchError(this.handleError)
      );
  }

  /**
   * Method to fetch the hourly forecastsMock for the city
   * @param options - params for the request
   * @return list of forecastsMock
   */
  getWeatherHourlyForecast(options = {}): Observable<ForecastHourly[]> {
    let prevDate;
    let currDate;
    let forecastHourly: ForecastHourly = { dt: 0, forecasts: []};
    const forecastsHourly: ForecastHourly[] = [];
    return this.http.get<any>(this.weatherHourlyUrl, {params: this.getHttpParams(options)})
      .pipe(
        map(({list = []}: {list?: Forecast[]} = {}) => {
          list.forEach((forecast: Forecast) => {
            currDate =  new Date(forecast.dt * 1000);
            if (prevDate && !this.sameDay(prevDate, currDate)) {
              forecastsHourly.push(forecastHourly);
              forecastHourly = { dt: 0, forecasts: [] };
            }
            forecastHourly.dt = forecast.dt;
            forecastHourly.forecasts.push(forecast);
            prevDate = currDate;
          });

          if (forecastHourly.dt !== 0 && forecastHourly.forecasts.length > 0) {
            forecastsHourly.push(forecastHourly);
          }
          return forecastsHourly;
        }),
        catchError(this.handleError)
      );
  }

  /**
   * Method to get the http params
   * @param id - list of city codes separated by comma
   * @param units - metric or imperial
   */
  private getHttpParams({ id, units = 'metric'}:
                          { id?: string, units?: string } = {}): HttpParams {
    return new HttpParams()
      .set('id', id)
      .set('units', units)
      .set('mode', 'json')
      .set('appid', environment.apiKey);
  }

  /**
   * Method to compare two dates
   */
  private sameDay(d1: Date, d2: Date) {
    return d1.getFullYear() === d2.getFullYear() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getDate() === d2.getDate();
  }

  /**
   * Method to handle error for each request
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
      return throwError('Network error; please try again later.');
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.error.message}`);
    }
    // return an observable with a user-facing error message
    return throwError('Technical error; please try again later.');
  }
}
