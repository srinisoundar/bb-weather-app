import { Injectable } from '@angular/core';

const ms2mph = 2.23694;
const windRange = {
  951: [0, 1.6],
  952: [1.6, 3.4],
  953: [3.4, 5.5],
  954: [5.5, 8],
  955: [8, 10.8],
  956: [10.8, 13.9],
  957: [13.9, 17.2],
  958: [17.2, 20.8],
  959: [20.8, 24.5],
  960: [24.5, 28.5],
  961: [28.5, 32.7],
  962: [32.7, Number.MAX_VALUE],
};

@Injectable({
  providedIn: 'root'
})
export class WindSpeedService {

  constructor() { }

   getWindScaleIcon(windSpeed: number, unit: string = 'ms'): string {
    let windIcon = '951';
    const convert = unit === 'ms' ? 1 : ms2mph;
    for (const key in windRange) {
      if (windSpeed >= windRange[key][0] * convert && windSpeed < windRange[key][1] * convert) {
        windIcon = key;
        break;
      }
    }
    return windIcon;
  }
}
