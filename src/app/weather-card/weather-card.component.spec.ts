import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherCardComponent } from './weather-card.component';
import {WindSpeedService} from '../shared/wind-speed.service';
import {Forecast} from '../shared/forecast.model';
import {forecastsMock} from '../shared/mock.forecast';
import Spy = jasmine.Spy;

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;
  let forecast: Forecast;
  let getWindScaleIconSpy: Spy;

  beforeEach(async(() => {
    const windSpeedService = jasmine.createSpyObj('WindSpeedService', ['getWindScaleIcon']);
    getWindScaleIconSpy = windSpeedService.getWindScaleIcon.and.returnValue('951');

    TestBed.configureTestingModule({
      declarations: [ WeatherCardComponent ],
      providers:    [
        { provide: WindSpeedService, useValue: windSpeedService }
      ]
    });
  }));

  beforeEach(() => {
    forecast = forecastsMock.list[0];
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the forecast details', () => {
    component.forecast = forecast;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.card-title').textContent).toBe(forecast.name);
    expect(compiled.querySelector('.owf-800').textContent).toBeDefined();
    expect(compiled.querySelector('.card-temp').textContent.trim()).toBe('31°C');
    expect(compiled.querySelector('.card-description').textContent.trim()).toBe(forecast.weather[0].description);
  });

  it('should initialize wind icon', () => {
    component.forecast = forecast;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(getWindScaleIconSpy.calls.any()).toBe(true, 'getWindScaleIcon called');
    expect(getWindScaleIconSpy).toHaveBeenCalledWith(forecast.wind.speed, 'ms');
    expect(compiled.querySelector('.owf-951').textContent).toBeDefined();
  });
});
