import {Component, Input, OnInit} from '@angular/core';

import {Forecast} from '../shared/forecast.model';
import {WindSpeedService} from '../shared/wind-speed.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {
  @Input() forecast: Forecast;

  constructor(public windSpeed: WindSpeedService) {
  }

  ngOnInit() {
  }
}
