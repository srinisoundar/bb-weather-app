import {Component, OnInit} from '@angular/core';
import {Forecast} from '../shared/forecast.model';

import {OpenWeatherMapService} from '../shared/open-weather-map.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  forecasts: Forecast[];
  loading: boolean;
  error: string;
  currDate: Date;

  constructor(private weatherService: OpenWeatherMapService) {
  }

  ngOnInit() {
    this.getGroupWeatherForecast();
  }

  /**
   * Method to get the weather forecast for cities
   */
  getGroupWeatherForecast(): void {
    const options = {
      id: '2759794,2988507,1726701,6359304,2800866',
      units: 'metric'
    };
    this.loading = true;
    this.weatherService.getGroupWeatherForecast(options)
      .subscribe(
        (forecasts: Forecast[]) => {
          this.forecasts = forecasts;
          this.currDate = forecasts.length > 0 && new Date(forecasts[0].dt * 1000);
        },
        (error) => this.error = error
      )
      .add(() => this.loading = false);
  }

  /**
   * Method to track the forecast by id
   * @return forecast id
   */
  trackByForecasts(index: number, forecast: Forecast): number {
    return forecast.id;
  }
}
