import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DashboardComponent} from './dashboard.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OpenWeatherMapService} from '../shared/open-weather-map.service';
import {of, throwError} from 'rxjs';
import {forecastsMock} from '../shared/mock.forecast';
import {ErrorAlertComponent} from '../shared/error-alert/error-alert.component';
import {WeatherCardComponent} from '../weather-card/weather-card.component';
import {RouterLinkDirectiveStub} from '../testing/router-link-directive-stub';
import {By} from '@angular/platform-browser';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let getGroupWeatherForecastSpy;

  beforeEach(async(() => {
    const openWeatherService = jasmine.createSpyObj('OpenWeatherMapService', ['getGroupWeatherForecast']);
    getGroupWeatherForecastSpy = openWeatherService.getGroupWeatherForecast.and.returnValue(of(forecastsMock.list));

    TestBed.configureTestingModule({
      declarations: [ DashboardComponent, ErrorAlertComponent, WeatherCardComponent, RouterLinkDirectiveStub ],
      providers: [{ provide: OpenWeatherMapService, useValue: openWeatherService }],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the component', () => {
    fixture.detectChanges();
    expect(getGroupWeatherForecastSpy).toHaveBeenCalledWith({id: '2759794,2988507,1726701,6359304,2800866', units: 'metric'});
    expect(component.loading).toBeFalsy();
    expect(component.forecasts).toBe(forecastsMock.list);
  });

  it('should render the weather forecast hourly details', () => {
    spyOn(component, 'trackByForecasts');
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.dashboard-title em').textContent).toBe('Jun 2, 2019');
    expect(compiled.querySelectorAll('.card-container').length).toBe(2, 'should have two cities');
    expect(component.trackByForecasts).toHaveBeenCalled();
  });

  it('should show error on getGroupWeatherForecast throws error', () => {
    getGroupWeatherForecastSpy.and.returnValue(throwError('network error'));
    fixture.detectChanges();
    expect(component.error).toBe('network error');
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.alert').textContent).toContain('network error');
  });

  it('can get RouterLinks from template', () => {
    fixture.detectChanges();
    const linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
    const routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].linkParams).toBe('/weather/2759794/Amsterdam');
    expect(routerLinks[1].linkParams).toBe('/weather/2988507/Paris');
  });
});
