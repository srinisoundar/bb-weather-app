import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

import {Forecast, ForecastHourly} from '../shared/forecast.model';
import {OpenWeatherMapService} from '../shared/open-weather-map.service';

@Component({
  selector: 'app-weather-hourly',
  templateUrl: './weather-hourly.component.html',
  styleUrls: ['./weather-hourly.component.scss']
})
export class WeatherHourlyComponent implements OnInit {
  loading: boolean;
  error: string;
  cityName: string;
  forecastsHourly: ForecastHourly[];

  constructor(
    private weatherService: OpenWeatherMapService,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.getWeatherHourlyForecast();
  }

  /**
   * Method to get the 3 hr / 5 day weather forecast
   */
  getWeatherHourlyForecast(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    const options = {
      id,
      units: 'metric'
    };
    this.cityName = this.route.snapshot.paramMap.get('name');
    this.loading = true;
    this.weatherService.getWeatherHourlyForecast(options)
      .subscribe(
        (forecasts: ForecastHourly[]) => this.forecastsHourly = forecasts,
        (error) => this.error = error
      )
      .add(() => this.loading = false);
  }

  /**
   * Method to go back to previous page
   */
  goBack(): void {
    this.location.back();
  }

  /**
   * Method to track the forecast group by date
   * @return forecast group date
   */
  trackByForecastHourly(index: number, forecastHourly: ForecastHourly ): number {
    return forecastHourly.dt;
  }

  /**
   * Method to track the forecast by date
   * @return forecast date
   */
  trackByForecasts(index: number, forecast: Forecast ): number {
    return forecast.dt;
  }
}
