import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeatherHourlyComponent} from './weather-hourly.component';
import {ActivatedRoute} from '@angular/router';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Location} from '@angular/common';
import {OpenWeatherMapService} from '../shared/open-weather-map.service';
import {of, throwError} from 'rxjs';
import Spy = jasmine.Spy;
import {forecastsHourlyMock} from '../shared/mock.forecast';
import {By} from '@angular/platform-browser';
import {ErrorAlertComponent} from '../shared/error-alert/error-alert.component';

describe('WeatherHourlyComponent', () => {
  let component: WeatherHourlyComponent;
  let fixture: ComponentFixture<WeatherHourlyComponent>;
  let getWeatherHourlyForecastSpy: Spy;
  let backSpy: Spy;
  const paramMap = new Map();
  let forecastsHourly;

  beforeEach(async(() => {
    forecastsHourly = [{
      dt: 1559800800,
      forecasts: [forecastsHourlyMock.list[0]]
    }];
    const openWeatherService = jasmine.createSpyObj('OpenWeatherMapService', ['getWeatherHourlyForecast']);
    getWeatherHourlyForecastSpy = openWeatherService.getWeatherHourlyForecast.and.returnValue(of(forecastsHourly));

    const location = jasmine.createSpyObj('Location', ['back']);
    backSpy = location.back;

    const activatedRouteSpy = jasmine.createSpy('ActivatedRoute')
      .and.returnValue({snapshot: { paramMap: { get: (id) => paramMap.get(id)}}});

    TestBed.configureTestingModule({
      declarations: [ WeatherHourlyComponent, ErrorAlertComponent ],
      providers:    [
        { provide: OpenWeatherMapService, useValue: openWeatherService },
        { provide: ActivatedRoute, useValue: activatedRouteSpy() },
        { provide: Location, useValue: location }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  }));



  beforeEach(() => {
    paramMap.set('id', '1234');
    paramMap.set('name', 'Amsterdam');
    fixture = TestBed.createComponent(WeatherHourlyComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the component', () => {
    fixture.detectChanges();
    expect(getWeatherHourlyForecastSpy).toHaveBeenCalledWith({id: 1234, units: 'metric'});
    expect(component.loading).toBeFalsy();
    expect(component.forecastsHourly).toBe(forecastsHourly);
    expect(component.cityName).toBe('Amsterdam', 'city param to be Amsterdam');
  });

  it('should render the weather forecast hourly details', () => {
    spyOn(component, 'trackByForecastHourly');
    spyOn(component, 'trackByForecasts');
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.weather-hourly-title span').textContent).toBe('Amsterdam');
    expect(compiled.querySelectorAll('table').length).toBe(1, 'should show one day');
    expect(compiled.querySelectorAll('tbody').length).toBe(1, 'should show one forecast');
    expect(component.trackByForecastHourly).toHaveBeenCalled();
    expect(component.trackByForecasts).toHaveBeenCalled();
  });

  it('should show error on getWeatherHourlyForecast throws error', () => {
    getWeatherHourlyForecastSpy.and.returnValue(throwError('network error'));
    fixture.detectChanges();
    expect(component.error).toBe('network error');
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.alert').textContent).toContain('network error');
  });

  it('should go back to previous page on back clicked', () => {
    fixture.detectChanges();
    const btn = fixture.debugElement.query(By.css('.btn'));
    btn.triggerEventHandler('click', null);
    expect(backSpy).toHaveBeenCalled();
  });

});
